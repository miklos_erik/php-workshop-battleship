<?php
/**
 * Created by PhpStorm.
 * User: erikmiklos
 * Date: 6/21/18
 * Time: 10:05 AM
 */

final class Board
{

    const MAX_NUMBER_OF_SHIPS = 5;

    private $ships = [];

    public  function __construct()
    {

    }

    /**
     * @param Player player$
     */
    public function save(Player $player)
    {

    }

    /**
     * @return array
     */
    public function getBattleField(): array
    {
        return $this->battleField;
    }

    /**
     * @param array $battleField
     */
    public function setBattleField(array $battleField): void
    {
        $this->battleField = $battleField;
    }

    /**
     * @return array
     */
    public function getLetterPosition(): array
    {
        return $this->letterPosition;
    }

    /**
     * @param array $letterPosition
     */
    public function setLetterPosition(array $letterPosition): void
    {
        $this->letterPosition = $letterPosition;
    }

    public function getWidth() : int
    {
        return self::BOARD_WIDTH;
    }

//    public function getPosition(int $px, string $py) : string
//    {
//        return $this->battleField[$px][$py];
//    }

    public function addShip(Ship $ship, array $position, string $orientation)
    {
        $ship->addPosition($position);
        $ship->addOrientation($orientation);

        if($this->shipExists($ship)) {
            return false;
        }

        foreach (Location::getLocationRange($ship) as $position) {
            if (!Location::isValid($position) || Location::isOverLapped($this->ships, $position)) {
                return false;
            }
        }

        $this->ships[] = $ship;
    }

    public function getShips() {
        return $this->ships;
    }

    public function shipExists(Ship $currentShip) {
        foreach ($this->getShips() as $ship) {
            if($ship->getName() === $currentShip->getName()) {
                return true;
            }
        }
        return false;
    }

}
