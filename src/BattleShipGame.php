<?php
/**
 * Created by PhpStorm.
 * User: erikmiklos
 * Date: 6/21/18
 * Time: 10:16 AM
 */

final class BattleShipGame
{
    /** @var int Number of players */
    const PLAYER_COUNT = 2;

    /** @var Board $board */
    private $board;

    /**
     * BattleShipGame constructor.
     * @param Board $board
     */
    public function __construct(Board $board)
    {
        $this->board = $board;
    }


    /**
     * @param array $position
     * @return string
     */
    public function shoot(array $position) : string
    {
        if(Location::isValid($position)) {
            $ship =  Location::getShipAtLocation($this->board->getShips(), $position);
            if($ship) {
                $ship->decrementHp();
                return "Hit. " . ucfirst($ship->getName());
            }
            return "Miss.";
        }
        return "Position invalid.";
//        return $this->board->getPosition($px, $py) !== "0";
    }


}
