<?php
/**
 * Created by PhpStorm.
 * User: erikmiklos
 * Date: 6/21/18
 * Time: 10:30 AM
 */

abstract class Ship
{
    private $name;
    private $size;

    private $position;
    private $hp;
    private $orientation;

    /**
     * Ship constructor.
     * @param $name
     * @param $size
     * @param $orientation
     */
    public function __construct($name, $size)
    {
        $this->name = $name;
        $this->size = $this->hp = $size;
    }

    public function addPosition(array $position)
    {
        $this->position = $position;
    }

    public function isDestroyed() : bool
    {
        return $this->hp > 0;
    }

    public function addOrientation(string $orientation)
    {
        $this->orientation = $orientation;
    }

    public function getHit()
    {
        $this->hp--;
    }

    public function getPosition() {
        return $this->position;
    }

    public function getOrientation() {
        return $this->orientation;
    }

    public function getSize() {
        return $this->size;
    }

    public function getName() {
        return $this->name;
    }

    public function decrementHp() {
        $this->hp--;
    }
}
