<?php
/**
 * Created by PhpStorm.
 * User: erikmiklos
 * Date: 6/21/18
 * Time: 10:11 AM
 */

final class Player
{


    public function __construct()
    {
        $this->setBoard(new Board());
    }

    /**
     * @return Board
     */
    public function getBoard(): Board
    {
        return $this->board;
    }

    /**
     * @param Board $board
     */
    public function setBoard(Board $board): void
    {
        $this->board = $board;
    }

    public function hit($position) : bool
    {

    }


}