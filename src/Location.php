<?php
/**
 * Created by PhpStorm.
 * User: tonytoth
 * Date: 21/06/2018
 * Time: 14:47
 */

final class Location
{
    /** @var int Width of the battle field */
    const  BOARD_WIDTH = 10;

    /** @var int Height of the battle field */
    const BOARD_HEIGHT = 10;

    public static function isOverLapped(array $ships, array $position) {
        foreach ($ships as $ship) {
            foreach(self::getLocationRange($ship) as $location) {
                if($location[0] === $position[0] && $location[1] === $position[1]) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function getShipAtLocation(array $ships, array $position) {
        foreach ($ships as $ship) {
            foreach(self::getLocationRange($ship) as $location) {
                if($location[0] === $position[0] && $location[1] === $position[1]) {
                    return $ship;
                }
            }
        }
        return null;
    }

    public static function isValid(array $position)
    {
        if ($position[0] > self::BOARD_WIDTH) {
            return false;
        }

        if (ord($position[1]) > ord("A") + self::BOARD_HEIGHT -1) {
            return false;
        }

        return true;
    }

    public static function getLocationRange(Ship $ship)
    {
        $positions = [$ship->getPosition()];

        if ($ship->getOrientation() == "h") {
            for ($i = 1; $i < $ship->getSize(); $i++) {
                $nextPosition = [$ship->getPosition()[0] + $i, $ship->getPosition()[1]];

                $positions[] = $nextPosition;
            }
        } elseif ($ship->getOrientation() == "v") {
            for ($i = 0; $i < $ship->getSize(); $i++) {
                $nextPosition = [$ship->getPosition()[0], chr(ord($ship->getPosition()[1]) + $i)];

                $positions[] = $nextPosition;
            }
        }

        return $positions;
    }
}
