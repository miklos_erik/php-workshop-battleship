<?php
/**
 * Created by PhpStorm.
 * User: tonytoth
 * Date: 21/06/2018
 * Time: 15:04
 */

class SubmarineShip extends Ship
{
    public function __construct()
    {
        parent::__construct("submarine", 3);
    }

}
