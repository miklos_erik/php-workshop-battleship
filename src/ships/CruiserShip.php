<?php
/**
 * Created by PhpStorm.
 * User: tonytoth
 * Date: 21/06/2018
 * Time: 15:06
 */

class CruiserShip extends Ship
{
    public function __construct()
    {
        parent::__construct("cruiser", 3);
    }

}
