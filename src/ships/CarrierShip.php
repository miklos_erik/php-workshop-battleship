<?php
/**
 * Created by PhpStorm.
 * User: tonytoth
 * Date: 21/06/2018
 * Time: 15:04
 */

class CarrierShip extends Ship
{
    public function __construct()
    {
        parent::__construct("carrier", 5);
    }
}
