<?php
/**
 * Created by PhpStorm.
 * User: tonytoth
 * Date: 21/06/2018
 * Time: 15:05
 */

class DestroyerShip extends Ship
{
    public function __construct()
    {
        parent::__construct("destroyer", 2);
    }
}
