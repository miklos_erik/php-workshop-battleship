<?php
/**
 * Created by PhpStorm.
 * User: tonytoth
 * Date: 21/06/2018
 * Time: 15:04
 */

class BattleShip extends Ship
{
    public function __construct()
    {
        parent::__construct("battleship", 4);
    }

}
