<?php
/**
 * Created by PhpStorm.
 * User: erikmiklos
 * Date: 6/21/18
 * Time: 11:29 AM
 */

use \PHPUnit\Framework\TestCase;

class BattleshipGameTest extends TestCase
{

    public function testMissedShoot()
    {
        $board = new Board();
        $board->addShip(new CruiserShip(), [5, 'A'], 'h');
        $board->addShip(new SubmarineShip(), [8, 'G'], 'h');

        $game = new BattleShipGame($board);

        $this->assertEquals("Miss.", $game->shoot([6,'C']));
    }

    public function testShoot()
    {
        $board = new Board();
        $board->addShip(new CruiserShip(), [5, 'A'], 'h');
        $board->addShip(new SubmarineShip(), [8, 'G'], 'h');

        $game = new BattleShipGame($board);

        $this->assertEquals("Hit. Submarine", $game->shoot([9,'G']));
    }

    public function testInvalidLocationShot() {
        $board = new Board();
        $board->addShip(new CruiserShip(), [5, 'A'], 'h');
        $board->addShip(new SubmarineShip(), [8, 'G'], 'h');

        $game = new BattleShipGame($board);

        $this->assertEquals("Position invalid.", $game->shoot([22,'X']));
    }
}
