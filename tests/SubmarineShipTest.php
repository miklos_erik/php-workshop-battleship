<?php
/**
 * Created by PhpStorm.
 * User: tonytoth
 * Date: 21/06/2018
 * Time: 15:15
 */


class SubmarineShipTest extends \PHPUnit\Framework\TestCase
{
    private $testShip;

    public function setUp()
    {
        $this->testShip = new SubmarineShip();
    }

    public function testShipName() {
        $this->assertEquals("submarine", $this->testShip->getName());
    }

    public function testShipSize() {
        $this->assertEquals(3, $this->testShip->getSize());
    }

}
