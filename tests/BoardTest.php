<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: erikmiklos
 * Date: 6/21/18
 * Time: 10:20 AM
 */

use \PHPUnit\Framework\TestCase;

class BoardTest extends TestCase
{

    public function testIfShipWasAdded()
    {
        $board = new Board();

        $ship = new CarrierShip();

        $board->addShip($ship, [4, "B"], "h");

        $this->assertTrue($board->shipExists($ship));
    }

    public function testIfShipDoesNotExists() {
        $board = new Board();

        $ship = new CarrierShip();

        $board->addShip($ship, [4, "B"], "h");

        $inExistentShip = new BattleShip();

        $this->assertFalse($board->shipExists($inExistentShip));
    }

    public function testGetLocationRange()
    {
        $ship = new DestroyerShip();

        $ship->addOrientation("h");
        $ship->addPosition([3,"C"]);

        $this->assertSame([[3,"C"],[4,"C"]], Location::getLocationRange($ship));
    }

    public function testIsPositionValid()
    {
        $board = new Board();

        $ship = new DestroyerShip();

        $board->addShip($ship, [3, "D"], "v");

        $this->assertTrue(Location::isValid([2,"D"]));

        $this->assertFalse(Location::isValid([11, "D"]));
        $this->assertFalse(Location::isValid([11, "W"]));
    }

    public function testLocationIsOverLapped() {
        $board  = new Board();
        $ship   = new CruiserShip();

        $board->addShip($ship, [4, 'B'], 'h');

        $this->assertTrue(Location::isOverLapped($board->getShips(), [4, 'B']));
    }

    public function testLocationIsNotOverLapped() {
        $board  = new Board();
        $ship   = new CruiserShip();

        $board->addShip($ship, [4, 'B'], 'h');

        $this->assertFalse(Location::isOverLapped($board->getShips(), [4, 'C']));
    }
}
