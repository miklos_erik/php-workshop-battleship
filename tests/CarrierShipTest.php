<?php
/**
 * Created by PhpStorm.
 * User: tonytoth
 * Date: 21/06/2018
 * Time: 15:08
 */


class CarrierShipTest extends \PHPUnit\Framework\TestCase
{
    private $testShip;

    public function setUp()
    {
        $this->testShip = new CarrierShip();
    }

    public function testShipName() {
        $this->assertEquals("carrier", $this->testShip->getName());
    }

    public function testShipSize() {
        $this->assertEquals(5, $this->testShip->getSize());
    }
}
