<?php
/**
 * Created by PhpStorm.
 * User: tonytoth
 * Date: 21/06/2018
 * Time: 15:16
 */


class DestroyerShipTest extends \PHPUnit\Framework\TestCase
{
    private $testShip;

    public function setUp()
    {
        $this->testShip = new DestroyerShip();
    }

    public function testShipName() {
        $this->assertEquals("destroyer", $this->testShip->getName());
    }

    public function testShipSize() {
        $this->assertEquals(2, $this->testShip->getSize());
    }

}
