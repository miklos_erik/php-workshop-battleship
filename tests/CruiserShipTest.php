<?php
/**
 * Created by PhpStorm.
 * User: tonytoth
 * Date: 21/06/2018
 * Time: 15:14
 */


class CruiserShipTest extends \PHPUnit\Framework\TestCase
{
    private $testShip;

    public function setUp()
    {
        $this->testShip = new CruiserShip();
    }

    public function testShipName() {
        $this->assertEquals("cruiser", $this->testShip->getName());
    }

    public function testShipSize() {
        $this->assertEquals(3, $this->testShip->getSize());
    }
}
