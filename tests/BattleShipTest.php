<?php
/**
 * Created by PhpStorm.
 * User: tonytoth
 * Date: 21/06/2018
 * Time: 15:13
 */


class BattleShipTest extends \PHPUnit\Framework\TestCase
{
    private $testShip;

    public function setUp()
    {
        $this->testShip = new BattleShip();
    }

    public function testShipName() {
        $this->assertEquals("battleship", $this->testShip->getName());
    }

    public function testShipSize() {
        $this->assertEquals(4, $this->testShip->getSize());
    }
}
